%% Classe theseUPPA.cls
%% -------------------
%%
%% Universite de Pau et des Pays de l'Adour
%%
%% This class was written from thesul class of Denis Roegel
%% ``http://www.loria.fr/~roegel/TeX/TUL.html``_ and is based on the class
%% book.
%%
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{thesuppa}[10/07/2014, classe pour une these UPPA]

% couleurs UPPA
\RequirePackage{Ucolors}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{thesul}}
\ProcessOptions
\LoadClass[a4paper, french]{thesul}

%%
%% set up some labels that will appears on the first page or headers
%%
\DFD{\'Ecole doctorale des sciences exactes et leurs applications}
\SetLab{Institut des Sciences Analytiques de de Physico-Chimie Pour
l'Environnement et les Matériaux --- UMR 5254}
\ThesisDomain{mention chimie}
% nom diplome par defaut
\ThesisDiploma{%
    \UseEntryFont{ThesisDiploma}%
    Doctorat de l'Universit\'e de Pau et des Pays de l'Adour\\[3mm]
    {\UseEntryFont{ThesisSpecialty}(\@ThesisDomain)}%
}
% prepare variables pour these
\newcommand\ThesisUPPA{%
    %\renewcommand{\@ThesisFirstPageHead}{\@ULhe@d}%
    \ThesisDiploma{%
        {\UseEntryFont{ThesisDiploma}%
        Doctorat de l'Universit\'e de Pau et des Pays de l'Adour\\[3mm]
        {\UseEntryFont{ThesisSpecialty}(\@ThesisDomain)}}%
    }%
}
% prepare variables pour HDR
\newcommand\HdrUPPA{%
    \ThesisInOrderToGet{pour l'obtention d'une}%
    %\renewcommand{\@ThesisFirstPageHead}{\@ULhe@d}%
    \ThesisDiploma{%
        {\UseEntryFont{ThesisDiploma}%
        Habilitation de l'Universit\'e de Pau et des Pays de l'Adour\\[3mm]
        {\UseEntryFont{ThesisSpecialty}(\@ThesisDomain)}}%
    }%
}
% prepare variables pour master
\newcommand\MasterUPPA{%
    \ClearJury
    \NewJuryCategory{Referents}{\textit{Enseignant r\'ef\'erent :}}{\textit{Enseignants r\'ef\'erents :}}
    \NewJuryCategory{Rapporteurs}{\textit{Rapporteur :}}{\textit{Rapporteurs :}}
    \NewJuryCategory{Encadrants}{\textit{Encadrant :}}{\textit{Encadrants :}}
    \ThesisKind{Rapport de stage}%
    %\renewcommand{\@ThesisFirstPageHead}{\@ULhe@d}%
    \ThesisPresentedThe{pr\'esent\'e le \@ThesisDate}%
    \ThesisDiploma{{\UseEntryFont{ThesisDiploma}%
    Master de l'Universit\'e de Pau et des Pays de l'Adour\\[3mm]
    {\UseEntryFont{ThesisSpecialty}(\@ThesisDomain)}}}}

% page de titre simple, on modifie simplement l'entête avec le logo de l'UPPA
% ThesisFirstPageHead modifie \@ThesisFirstPageHead qui est en fait \@ULhe@d
% Voir ligne 130 de la classe thesul
\newcommand\UPPALogo[1]{\def\@UPPALogo{#1}}
\UPPALogo{%
    \vtop to0pt{\hbox{}\hbox to0pt{\includegraphics[width=5cm]{LogoUPPAcouleurCMJN}\hss}\vss}%
}
% ici je reprend la syntaxe de \@ULhe@d ... sans conviction :'(
\ThesisFirstPageHead{%
    {\UseEntryFont{ThesisFirstPageHead}\noindent
    \centerline{{\setbox0=\hbox{$\raise1.5cm\hbox{\@UPPALogo}$}%
                     \ht0=\baselineskip\box0}\hfill\@DFD}}%
    \@TUL@cmn@head\par
}
% pied de page, ligne 145 thesul.cls
\ThesisFirstPageFoot{%
    \hrule
    \vskip2mm
    \begin{center}\UseEntryFont{ThesisFirstPageFoot}%
        \the\@lab@tok
    \end{center}
}

% nom de la classe après la page de titre. Message codé dans thesul.cls ?
%\DontWriteClassName permet de ne rien afficher
\renewcommand\@dcd{}
\renewcommand\@p@ndor@{Mise en page avec la classe thesuppa}
\newcommand\DontWriteClassName{\renewcommand\@p@ndor@{}}

% Page de titre en couleur, dans le style de la charte UPPA 2014
\RequirePackage{tikz}
\usetikzlibrary{calc}
\newcommand{\MakeFancyThesisTitlePage}[1][Ubleuclair]{{%
    \newpage
    \thispagestyle{empty}
    \renewcommand{\TitlePageFontFamily}{phv}
    \renewcommand{\TitlePageFontEncoding}{OT1}
    \SetTULFont{ThesisTitle}%
                 {\TitlePageFontEncoding}{\TitlePageFontFamily}{m}{n}{26}{30}%
    \SetTULFont{ThesisKind}%
                 {\TitlePageFontEncoding}{\TitlePageFontFamily}{m}{n}{24}{28}%
    \SetTULFont{ThesisPresentedThe}%
                 {\TitlePageFontEncoding}{\TitlePageFontFamily}{m}{n}{12}{14}%
    \SetTULFont{ThesisInOrderToGet}%
                 {\TitlePageFontEncoding}{\TitlePageFontFamily}{m}{n}{12}{14}%
    \SetTULFont{ThesisDiploma}%
                 {\TitlePageFontEncoding}{\TitlePageFontFamily}{m}{n}{12}{14}%
    \SetTULFont{ThesisSpecialty}%
                 {\TitlePageFontEncoding}{\TitlePageFontFamily}{n}{n}{14}{18}%
    \SetTULFont{ThesisPresentedBy}%
                 {\TitlePageFontEncoding}{\TitlePageFontFamily}{m}{n}{14}{18}%
    \SetTULFont{ThesisAuthor}%
                 {\TitlePageFontEncoding}{\TitlePageFontFamily}{b}{n}{20}{24}%
    \SetTULFont{ThesisJuryTitle}%
                 {\TitlePageFontEncoding}{\TitlePageFontFamily}{b}{n}{12}{13}%
    \SetTULFont{ThesisJury}%
                 {\TitlePageFontEncoding}{\TitlePageFontFamily}{m}{n}{12}{16}%
    \SetTULFont{ThesisUniv}%
                 {\TitlePageFontEncoding}{\TitlePageFontFamily}{b}{n}{14}{18}%
    \SetTULFont{ThesisDFD}%
                 {\TitlePageFontEncoding}{\TitlePageFontFamily}{m}{n}{14}{18}%
    \begin{tikzpicture}[remember picture, overlay, font=\sffamily]
        \useasboundingbox (current page.north east) rectangle (current page.south west);
        \node[anchor=north west,
              fill=Ugrisfonce,
              text width=\paperwidth,
              minimum height=4.8cm,
              align=center,
              text=white] at (current page.north west) (head) {%
                  \@ThesisKind
                  \vskip3mm plus2fil
                  \UseEntryFont{ThesisUniv}
                  UNIVERSITÉ DE PAU ET DES PAYS DE L'ADOUR
                  \vskip3mm plus2fil
                  \UseEntryFont{ThesisDFD}
                  \@DFD
              };
        \fill[color=#1] ($(head.south west) + (0pt, 1pt)$)
            -- ($(head.south east) + (0pt, 1pt)$)
            -- ($(head.south east) - (0cm, 10cm)$)
            -- ($(head.south west) - (0cm, 12cm)$) -- cycle;
        \node[anchor=north west,
              text width=\paperwidth,
              %minimum height=12cm,
              align=center,
              text=white] at (head.south west) {%
                  \vskip5mm plus2fil
                      \@ThesisPresentedThe
                  \vskip2mm plus2fil
                      \@ThesisPresentedBy{} \@ThesisAuthor
                  \vskip10mm plus2fil
                      \@ThesisInOrderToGet
                  \vskip1mm plus2fil
                      \@ThesisDiploma
                  \vskip10mm plus2fil
                      \@ThesisTitle
              };
        \node[#1] at ($(head.south) - (0cm, 12cm)$) {%
            \UseEntryFont{ThesisJuryTitle}
            COMPOSITION DU JURY
            %\@ThesisJuryTitle
        };
        \node[anchor=north west,
              text width=\paperwidth,
              align=left] at ($(head.south west) - (0cm, 13cm)$) {%
                  \hspace{2cm}\@ThesisJury
              };
        \node[anchor=south, yshift=1cm] at (current page.south) {%
            \includegraphics[width=7cm]{LogoUPPAcouleurCMJN}};
    \end{tikzpicture}
    \newpage
    \if@twoside
       \thispagestyle{empty}
       \hbox{}
       \par\vfill\@dcd\@p@ndor@
       \newpage
       \addtocounter{page}{-2}%
    \else
       \addtocounter{page}{-1}%
    \fi
}}

% Page de contact, pour la fin du manuscript
\newcommand{\MakeContactPage}[2][Ubleuclair]{{%
    \newpage
    \thispagestyle{empty}
    \renewcommand{\TitlePageFontFamily}{phv}
    \renewcommand{\TitlePageFontEncoding}{OT1}
    \SetTULFont{ThesisDFD}%
                 {\TitlePageFontEncoding}{\TitlePageFontFamily}{m}{n}{12}{14}%
    \SetTULFont{ThesisAuthor}%
                 {\TitlePageFontEncoding}{\TitlePageFontFamily}{b}{n}{14}{18}%
    \begin{tikzpicture}[remember picture, overlay, font=\sffamily]
        \useasboundingbox (current page.north east) rectangle (current page.south west);
        \node[anchor=north west,
              fill=Ugrisfonce,
              text width=\paperwidth,
              minimum height=4.8cm,
              align=center,
              text=white] at (current page.north west) (head) {
                  \UseEntryFont{ThesisDFD}
                  ÉCOLE DOCTORALE :\\
                  \@DFD

                  \vskip1cm plus2fil
                  {LABORATOIRE :}\\
                  \the\@lab@tok
              };
        \fill[color=#1] ($(head.south west) + (0pt, 1pt)$)
            -- ($(head.south east) + (0pt, 1pt)$)
            -- ($(head.south east) - (0cm, 18cm)$)
            -- ($(head.south west) - (0cm, 16cm)$) -- cycle;
        \node[text width=.8\paperwidth,
              align=center,
              text=white] at (current page.center) {%
                  \usefont{OT1}{phv}{m}{n}
                  %{\Large\textbf{CONTACT}}

                  \vskip5mm plus2fil
                  \@ThesisAuthor

                  \vskip5mm plus2fil
                  {\fontsize{12}{18}\selectfont#2}
              };
        \node[anchor=south, yshift=1cm] at (current page.south) {%
            \includegraphics[width=5.5cm]{LogoUPPAcouleurURL}};
    \end{tikzpicture}
}}
% Page de contact, pour la fin du manuscript (Stage et TER Master)
\newcommand{\MakeContactPageMaster}[2][Ubleuclair]{{%
    \newpage
    \thispagestyle{empty}
    \renewcommand{\TitlePageFontFamily}{phv}
    \renewcommand{\TitlePageFontEncoding}{OT1}
    \SetTULFont{ThesisDFD}%
                 {\TitlePageFontEncoding}{\TitlePageFontFamily}{m}{n}{12}{14}%
    \SetTULFont{ThesisAuthor}%
                 {\TitlePageFontEncoding}{\TitlePageFontFamily}{b}{n}{14}{18}%
    \begin{tikzpicture}[remember picture, overlay, font=\sffamily]
        \useasboundingbox (current page.north east) rectangle (current page.south west);
        \node[anchor=north west,
              fill=Ugrisfonce,
              text width=\paperwidth,
              minimum height=4.8cm,
              align=center,
              text=white] at (current page.north west) (head) {
                  \UseEntryFont{ThesisDFD}
                  \@DFD

                  \vskip1cm plus2fil
                  \the\@lab@tok
              };
        \fill[color=#1] ($(head.south west) + (0pt, 1pt)$)
            -- ($(head.south east) + (0pt, 1pt)$)
            -- ($(head.south east) - (0cm, 18cm)$)
            -- ($(head.south west) - (0cm, 16cm)$) -- cycle;
        \node[text width=.8\paperwidth,
              align=center,
              text=white] at (current page.center) {%
                  \usefont{OT1}{phv}{m}{n}
                  %{\Large\textbf{CONTACT}}

                  \vskip5mm plus2fil
                  \@ThesisAuthor

                  \vskip5mm plus2fil
                  {\fontsize{12}{18}\selectfont#2}
              };
        \node[anchor=south, yshift=1cm] at (current page.south) {%
            \includegraphics[width=5.5cm]{LogoUPPAcouleurURL}};
    \end{tikzpicture}
}}
\endinput
